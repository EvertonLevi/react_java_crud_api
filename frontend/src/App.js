import React from 'react';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter,
  Form, FormGroup, Input, Label
} from 'reactstrap';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addFunc: false,
      editarFuncionario: false,
      todosFuncionarios: [],
      response: {},
      idEdit: Number,
      modalIsOpen: false,
      modalIsOpenEdit: false,
    }
  }

  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value })
  }
  submitHandler = e => {
    axios.post('http://localhost:8080/WebServiceRest/rest/service/cadastrar', {
      nome: this.state.nome,
      sobrenome: this.state.sobrenome,
      email: this.state.email,
      nis: this.state.nis
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  toggleModalEdit = async (e) => {
    this.setState({
      modalIsOpenEdit: !this.state.modalIsOpenEdit,
    })
    this.setState({ idEdit: e })
  }
  editarFuncionario = () => {
    let obj = {
      id: this.state.idEdit,
      nome: this.state.nome,
      sobrenome: this.state.sobrenome,
      email: this.state.email,
      nis: this.state.nis
    }
    axios.put('http://localhost:8080/WebServiceRest/rest/service/alterar',
      obj
    )
      .then(result => {
        console.log(result.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  deletarFuncionario(id) {
    const { todosFuncionarios } = this.state;
    axios.delete('http://localhost:8080/WebServiceRest/rest/service/excluir/' + id)
      .then(result => {
        this.setState({
          response: result,
          todosFuncionarios: todosFuncionarios.filter(funcionario => funcionario.id !== id)
        })
      })
  }

  toggleModal = () => {
    this.setState({
      modalIsOpen: !this.state.modalIsOpen
    })
  }


  componentDidMount = async () => {
    fetch("http://localhost:8080/WebServiceRest/rest/service/todosFuncionarios"
    )
      .then((res) => res.json())
      .then((findres) => {
        this.setState({
          todosFuncionarios: findres,
        })
      })
      .catch(error => {
        alert('Houve algum erro :( ' + error)
      })
  }

  render() {
    return (

      <div className="App" >
        <div className="container">
          <div>
          </div>

          <table className="table table-dark">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Sobrenome</th>
                <th>E-mail</th>
                <th>NIS</th>
                <th> </th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              {this.state.todosFuncionarios.map(
                func =>
                  <tr key={func.id}>
                    <td>{func.nome}</td>
                    <td>{func.sobrenome}</td>
                    <td>{func.email}</td>
                    <td>{func.nis}</td>
                    <td>
                      <button type='button' className="btn btn-danger"
                        onClick={() => this.deletarFuncionario(func.id)}
                      >Deletar</button>
                    </td>
                    <td>
                      <button type='button' className="btn btn-primary"
                        onClick={() => this.toggleModalEdit(func.id)}>Editar</button>
                    </td>
                  </tr>
              )}
            </tbody>
          </table>
          <button type='button'
            onClick={this.toggleModal}
            className="btn btn-success">Inserir novo funcionário</button>
        </div>
        <div>
        </div>
        <Modal isOpen={this.state.modalIsOpen}>
          <ModalHeader
          >CADASTRAR FUNCIONÁRIO</ModalHeader>
          <ModalBody>
            <Form onSubmit={this.submitHandler}>
              <FormGroup>
                <Label  >Nome</Label>
                <Input type="text" required="required"  pattern="[a-zA-Z]{2,30}"
                title="Use entre 2 e 30 caracteres" placeholder="Insira seu nome..." name="nome" value={this.nome}
                  onChange={this.changeHandler} />
              </FormGroup>
              <FormGroup>
                <Label  >Sobrenome</Label>
                <Input type="text" required="required"  pattern="[a-zA-Z]{2,50}"
                title="Use entre 2 e 50 caracteres" placeholder="Insira seu sobrenome..." name="sobrenome" value={this.sobrenome}
                  onChange={this.changeHandler} />
              </FormGroup>
              <FormGroup>
                <Label  >Email</Label>
                <Input type="email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" value={this.email} placeholder="Insira seu e-mail..."
                  onChange={this.changeHandler} />
              </FormGroup>
              <FormGroup>
                <Label  >NIS</Label>
                <Input type="number" pattern="[0-9]{11,13}+$" mi required="required" placeholder="Insira seu NIS..." name="nis" value={this.nis}
                  onChange={this.changeHandler} />
              </FormGroup>
              <Button color="primary" type="submit" >Salvar</Button>
              <Button color="secondary" type="button" onClick={this.toggleModal.bind(this)}>Cancelar</Button>
            </Form>
          </ModalBody>
          <ModalFooter>
          </ModalFooter>
        </Modal>
        <div>
          <Modal isOpen={this.state.modalIsOpenEdit}>
            <ModalHeader
            >EDITAR FUNCIONÁRIO</ModalHeader>
            <ModalBody>
              <Form onSubmit={this.editarFuncionario}>
                <FormGroup>
                  <Label  >Nome</Label>
                  <Input type="text" required="required"  pattern="[a-zA-Z]{2,30}" 
                  title="Use entre 2 e 30 caracteres" placeholder="Insira seu nome..." name="nome" value={this.state.nome}
                    onChange={e => this.setState({ nome: e.target.value })}
                  />
                </FormGroup>
                <FormGroup>
                  <Label  >Sobrenome</Label>
                  <Input type="text" required="required" pattern="[a-zA-Z]{2,50}"
                   title="Use entre 2 e 50 caracteres" placeholder="Insira seu sobrenome..." name="sobrenome" value={this.state.sobrenome}
                    onChange={e => this.setState({ sobrenome: e.target.value })}
                  />
                </FormGroup>
                <FormGroup>
                  <Label  >Email</Label>
                  <Input type="email" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" value={this.state.email} placeholder="Insira seu e-mail..."
                    onChange={e => this.setState({ email: e.target.value })}
                  />
                </FormGroup>
                <FormGroup>
                  <Label  >NIS</Label>
                  <Input type="number" pattern="[0-9]{11,13}+$" required="required" placeholder="Insira seu NIS..." name="nis" value={this.state.nis}
                    onChange={e => this.setState({ nis: e.target.value })}
                  />
                </FormGroup>
                <Button color="primary" type="submit" >Salvar</Button>
                <Button color="secondary" type="button" onClick={this.toggleModalEdit.bind(this)}>Cancelar</Button>
              </Form>
            </ModalBody>
            <ModalFooter>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}

export default App;
