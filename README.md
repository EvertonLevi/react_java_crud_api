# react_java_crud_api

CRUD utilizando React no front e Java no WS.

# Teste desenvolvedor 

![](https://miro.medium.com/max/3308/1*UCZP7y-Iv1GItLJAO-5nxw.png)
 
###Escopo do desafio
                
+ Linguagem back-end: Java
+ Linguagem front-end: HTML5 com React, Angular ou ExtJS
+ Do projeto
    + Projeto tem o objetivo de efetuar um CRUD de funcionário
+ Dos atributos do Funcionário
	+ Nome (entre 2 e 30 caracteres)
	+ Sobrenome (entre 2 e 50 caracteres)
	+ e-mail (validar e-mail)
	+ Número do NIS (PIS) (somente números)
+ Das funcionalidades (Utilizando APIs)
	+ Inserção de um Funcionário
	+ Exclusão de um Funcionário
	+ Atualização de um Funcionário
	+ Listagem de um Funcionário
+ Do desenvolvimento:
	+ Utilize o padrão de desenvolvimento que acredita ser o mais correto.
+ Da entrega dos fontes:
	+ Faça a entrega dos fontes da forma que acredita ser a mais correta.
 
Instalação
=============

###Frontend
                
1. Clone o repositório
2. Abra a pasta frontend
`cd frontend`
3. Instale as dependências
`npm install` ou `yarn`
4. Abra o projeto no browser
`npm start` ou `yarn start`
5. Rode o servidor (projeto feito com o xampp)

                
###Backend

1. Crie um banco chamado 'ciss' (projeto criado no PHPMyAdmin)
2. Abra o projeto em sua ide (projeto feito em Eclipse 2019-09)
3. Configure o Tomcat v7
4. Click com o botão direito na pasta do projeto
5. Click em 'run and server'

                
###Arquitetura
  ![](http://2.bp.blogspot.com/-reL9II5qypo/T8jbxWfbxTI/AAAAAAAAAXA/4qx0wv05uAk/s1600/cliente-servidor.jpg)
  
                  
###Padrão
  + O Padrão escolhido para o projeto foi o Template Method;
  	+ Projeto em pequeno porte. A inserção do padrão será ao decorrer do crescimento do projeto, considerando os conhecimentos atuais para.



###Fim :)