package br.com.http;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Funcionario {
	
	private int id, nis;
	private String nome, sobrenome, email;
	
	public Funcionario() {
		
	}
	
	public Funcionario(int id, String nome, String sobrenome, String email, int nis) {
		super();
		this.id = id;
		this.nis = nis;
		this.email = email;
		this.nome = nome;
		this.sobrenome = sobrenome;
	}
	
	public Funcionario(int id, String nome, String email) {
		super();
		this.id = id;
		this.nis = nis;
		this.email = email;
		this.nome = nome;
		this.sobrenome = sobrenome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNis() {
		return nis;
	}

	public void setNis(int nis) {
		this.nis = nis;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

}
