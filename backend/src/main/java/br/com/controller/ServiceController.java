package br.com.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.Header;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.omg.CORBA.portable.ApplicationException;

import br.com.http.Funcionario;
import br.com.repository.FuncionarioRepository;
import br.com.repository.entity.FuncionarioEntity;
 
 
/**
 * Essa classe vai expor os nossos métodos para serem acessasdos via http
 * @Path - Caminho para a chamada da classe que vai representar o nosso serviço
 * */
@Path("/service")
public class ServiceController {
 
	private final  FuncionarioRepository repository = new FuncionarioRepository();
 
 
 
	
	/**
	 * @Consumes - determina o formato dos dados que vamos postar
	 * @Produces - determina o formato dos dados que vamos retornar
	 * 
	 * Esse método cadastra uma nova funcionario
	 * */
	@POST	
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/cadastrar")
	public String Cadastrar(Funcionario funcionario){
 
		FuncionarioEntity entity = new FuncionarioEntity();
 
		try {
 
			entity.setNome(funcionario.getNome());
			entity.setSobrenome(funcionario.getSobrenome());
			entity.setEmail(funcionario.getEmail());
			entity.setNis(funcionario.getNis());
 
			repository.salvar(entity);
 
			return "Registro cadastrado com sucesso!";
 
		} catch (Exception e) {
 
			return "Erro ao cadastrar um registro " + e.getMessage();
		}

	}
 
	/**
	 * Essse método altera uma funcionario já cadastrada
	 * **/
	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")	
	@Path("/alterar")
	public String Alterar(Funcionario funcionario){
 		FuncionarioEntity entity = new FuncionarioEntity();
		try {
			entity.setId(funcionario.getId());
			entity.setNome(funcionario.getNome());
			entity.setSobrenome(funcionario.getSobrenome());
			entity.setEmail(funcionario.getEmail());
			entity.setNis(funcionario.getNis());
			repository.alterar(entity);
			return "Registro alterado com sucesso!";
 
		} catch (Exception e) {

			return "Erro ao alterar o registro " + e.getMessage();
		}
 
	}
	/**
	@Produces("application/json; charset=UTF-8")
	 * Esse método lista todas funcionarios cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/todosFuncionarios")
	public List<Funcionario> TodosFuncionarios() throws IOException, ApplicationException{
 
//		JsonArrayBuilder array = Json.createArrayBuilder();
		List<Funcionario> funcionarios =  new ArrayList<Funcionario>();
 
		List<FuncionarioEntity> listaEntityFuncionarios = repository.todosFuncionarios();
 
		for (FuncionarioEntity entity : listaEntityFuncionarios) {
 
			funcionarios.add(new Funcionario(entity.getId(), entity.getNome(), entity.getSobrenome(),entity.getEmail(), entity.getNis()));
		}
		return funcionarios;
	}
 
	/**
	 * Esse método busca uma funcionario cadastrada pelo código
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/getFuncionario/{id}")
	public Funcionario GetFuncionario(@PathParam("id") Integer id){
 
		FuncionarioEntity entity = repository.getFuncionario(id);
 
		if(entity != null)
			return new Funcionario(entity.getId(), entity.getNome(), entity.getSobrenome(),entity.getEmail(),entity.getNis());
		return null;
	}
 
	/**
	 * Excluindo uma funcionario pelo código
	 * */
	@DELETE
	@Produces("application/json; charset=UTF-8")
	@Path("/excluir/{id}")	
	public String excluir(@PathParam("id") Integer id){
 
		try {
			repository.excluir(id);
			return "Registro excluido com sucesso!";
		} catch (Exception e) {
			return "Erro ao excluir o registro! " + e.getMessage();
		}
 
	}
 
}