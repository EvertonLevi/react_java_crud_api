package br.com.repository;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.repository.entity.FuncionarioEntity;

public class FuncionarioRepository {

	private final EntityManagerFactory entityManagerFactory;

	private final EntityManager entityManager;

	public FuncionarioRepository(){

		/*CRIANDO O NOSSO EntityManagerFactory COM AS PORPRIEDADOS DO ARQUIVO persistence.xml */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("test");

		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS
	 * */
	public void salvar(FuncionarioEntity funcionarioEntity){

		this.entityManager.getTransaction().begin();
		this.entityManager.persist(funcionarioEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * ALTERA UM REGISTRO CADASTRADO
	 * */
	public void alterar(FuncionarioEntity funcionarioEntity){

		this.entityManager.getTransaction().begin();
		this.entityManager.merge(funcionarioEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * RETORNA TODAS AS FUNCIONARIOS CADASTRADAS NO BANCO DE DADOS 
	 * */
	@SuppressWarnings("unchecked")
	public List<FuncionarioEntity> todosFuncionarios(){

		return this.entityManager.createQuery("SELECT f FROM FuncionarioEntity f ORDER BY f.nome").getResultList();
	}

	/**
	 * CONSULTA UMA FUNCIONARIO CADASTRA PELO CÓDIGO
	 * */
	public FuncionarioEntity getFuncionario(Integer id){

		return this.entityManager.find(FuncionarioEntity.class, id);
	}

	/**
	 * EXCLUINDO UM REGISTRO PELO CÓDIGO
	**/
	public void excluir(Integer id){

		FuncionarioEntity funcionario = this.getFuncionario(id);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(funcionario);
		this.entityManager.getTransaction().commit();

	}
}